const getDrink = async () => {
    const drink = document.getElementById("nombre").value;
    console.log(drink)
    if (!drink) {
      alert("Ingrese el nombre de una bebida");
      limpiar();
      return;
    }
    const url = 'https://www.thecocktaildb.com/api/json/v1/1/search.php?s='+drink;
    console.log(url);
    axios.get(url)
    .then(response => {
        const bebida = response.data
        if (bebida.drinks[0].strDrink.toLowerCase() != drink.toLowerCase()) {
            alert("No hay ninguna bebida con ese nombre");
            limpiar();
            return;
        }
        document.getElementById('nombre').value = bebida.drinks[0].strDrink;
        document.getElementById('kind').value = bebida.drinks[0].strCategory;
        document.getElementById('steps').value = bebida.drinks[0].strInstructions;
        document.getElementById('imagen').style.backgroundImage = "url('"+bebida.drinks[0].strDrinkThumb+"')";
    })
    .catch(error => {
        console.error(error);
        alert("Hubo un error al buscar la bebida");
        limpiar();
    })
  };
  
const limpiar = () =>{
    document.getElementById('nombre').value = "";
    document.getElementById('kind').value = "";
    document.getElementById('steps').value = "";
    document.getElementById('imagen').style.backgroundImage = 'url()';
}